// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.13.0
// source: v1/order_private/pricing_private.proto

package order_private

import (
	order "gitlab.com/fcpartners/apis/gen/order/v1/order"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

//-----------------------
// UpdatePriceListRequest
//-----------------------
// Update status only
type UpdatePriceListRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Process order.ProductProcess `protobuf:"varint,2,opt,name=process,proto3,enum=fcp.order.v1.order.ProductProcess" json:"process,omitempty"`
	Type    order.ProductType    `protobuf:"varint,3,opt,name=type,proto3,enum=fcp.order.v1.order.ProductType" json:"type,omitempty"`
	Status  order.Pricing_Status `protobuf:"varint,6,opt,name=status,proto3,enum=fcp.order.v1.order.Pricing_Status" json:"status,omitempty"`
	UserId  string               `protobuf:"bytes,5,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
}

func (x *UpdatePriceListRequest) Reset() {
	*x = UpdatePriceListRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_order_private_pricing_private_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdatePriceListRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdatePriceListRequest) ProtoMessage() {}

func (x *UpdatePriceListRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_order_private_pricing_private_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdatePriceListRequest.ProtoReflect.Descriptor instead.
func (*UpdatePriceListRequest) Descriptor() ([]byte, []int) {
	return file_v1_order_private_pricing_private_proto_rawDescGZIP(), []int{0}
}

func (x *UpdatePriceListRequest) GetProcess() order.ProductProcess {
	if x != nil {
		return x.Process
	}
	return order.ProductProcess_UNSPECIFIED
}

func (x *UpdatePriceListRequest) GetType() order.ProductType {
	if x != nil {
		return x.Type
	}
	return order.ProductType_ProductType_UNSPECIFIED
}

func (x *UpdatePriceListRequest) GetStatus() order.Pricing_Status {
	if x != nil {
		return x.Status
	}
	return order.Pricing_ACTIVE
}

func (x *UpdatePriceListRequest) GetUserId() string {
	if x != nil {
		return x.UserId
	}
	return ""
}

type UpdatePriceListResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *UpdatePriceListResponse) Reset() {
	*x = UpdatePriceListResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_order_private_pricing_private_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdatePriceListResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdatePriceListResponse) ProtoMessage() {}

func (x *UpdatePriceListResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_order_private_pricing_private_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdatePriceListResponse.ProtoReflect.Descriptor instead.
func (*UpdatePriceListResponse) Descriptor() ([]byte, []int) {
	return file_v1_order_private_pricing_private_proto_rawDescGZIP(), []int{1}
}

//-----------------------
// DeletePriceListRequest
//-----------------------
type DeletePriceListRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Process order.ProductProcess `protobuf:"varint,1,opt,name=process,proto3,enum=fcp.order.v1.order.ProductProcess" json:"process,omitempty"`
	Type    order.ProductType    `protobuf:"varint,2,opt,name=type,proto3,enum=fcp.order.v1.order.ProductType" json:"type,omitempty"`
	UserId  string               `protobuf:"bytes,4,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
}

func (x *DeletePriceListRequest) Reset() {
	*x = DeletePriceListRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_order_private_pricing_private_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeletePriceListRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeletePriceListRequest) ProtoMessage() {}

func (x *DeletePriceListRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_order_private_pricing_private_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeletePriceListRequest.ProtoReflect.Descriptor instead.
func (*DeletePriceListRequest) Descriptor() ([]byte, []int) {
	return file_v1_order_private_pricing_private_proto_rawDescGZIP(), []int{2}
}

func (x *DeletePriceListRequest) GetProcess() order.ProductProcess {
	if x != nil {
		return x.Process
	}
	return order.ProductProcess_UNSPECIFIED
}

func (x *DeletePriceListRequest) GetType() order.ProductType {
	if x != nil {
		return x.Type
	}
	return order.ProductType_ProductType_UNSPECIFIED
}

func (x *DeletePriceListRequest) GetUserId() string {
	if x != nil {
		return x.UserId
	}
	return ""
}

type DeletePriceListResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *DeletePriceListResponse) Reset() {
	*x = DeletePriceListResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_order_private_pricing_private_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeletePriceListResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeletePriceListResponse) ProtoMessage() {}

func (x *DeletePriceListResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_order_private_pricing_private_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeletePriceListResponse.ProtoReflect.Descriptor instead.
func (*DeletePriceListResponse) Descriptor() ([]byte, []int) {
	return file_v1_order_private_pricing_private_proto_rawDescGZIP(), []int{3}
}

var File_v1_order_private_pricing_private_proto protoreflect.FileDescriptor

var file_v1_order_private_pricing_private_proto_rawDesc = []byte{
	0x0a, 0x26, 0x76, 0x31, 0x2f, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x70, 0x72, 0x69, 0x76, 0x61,
	0x74, 0x65, 0x2f, 0x70, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x72, 0x69, 0x76, 0x61,
	0x74, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1a, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x70, 0x72, 0x69,
	0x76, 0x61, 0x74, 0x65, 0x1a, 0x13, 0x76, 0x31, 0x2f, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2f, 0x65,
	0x6e, 0x75, 0x6d, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1c, 0x76, 0x31, 0x2f, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x5f, 0x70, 0x72, 0x69, 0x63, 0x69, 0x6e,
	0x67, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xec, 0x01, 0x0a, 0x16, 0x55, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x50, 0x72, 0x69, 0x63, 0x65, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x3c, 0x0a, 0x07, 0x70, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x0e, 0x32, 0x22, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e,
	0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74,
	0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x52, 0x07, 0x70, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73,
	0x12, 0x33, 0x0a, 0x04, 0x74, 0x79, 0x70, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x1f,
	0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x2e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x54, 0x79, 0x70, 0x65, 0x52,
	0x04, 0x74, 0x79, 0x70, 0x65, 0x12, 0x3a, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x22, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65,
	0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x50, 0x72, 0x69, 0x63, 0x69,
	0x6e, 0x67, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x12, 0x17, 0x0a, 0x07, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x05, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x4a, 0x04, 0x08, 0x01, 0x10, 0x02,
	0x4a, 0x04, 0x08, 0x04, 0x10, 0x05, 0x22, 0x19, 0x0a, 0x17, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x50, 0x72, 0x69, 0x63, 0x65, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x22, 0xaa, 0x01, 0x0a, 0x16, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x50, 0x72, 0x69, 0x63,
	0x65, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x3c, 0x0a, 0x07,
	0x70, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x22, 0x2e,
	0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x2e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73,
	0x73, 0x52, 0x07, 0x70, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x12, 0x33, 0x0a, 0x04, 0x74, 0x79,
	0x70, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x1f, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x50, 0x72,
	0x6f, 0x64, 0x75, 0x63, 0x74, 0x54, 0x79, 0x70, 0x65, 0x52, 0x04, 0x74, 0x79, 0x70, 0x65, 0x12,
	0x17, 0x0a, 0x07, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x4a, 0x04, 0x08, 0x03, 0x10, 0x04, 0x22, 0x19,
	0x0a, 0x17, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x50, 0x72, 0x69, 0x63, 0x65, 0x4c, 0x69, 0x73,
	0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x32, 0x8d, 0x12, 0x0a, 0x0e, 0x50, 0x72,
	0x69, 0x63, 0x69, 0x6e, 0x67, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x65, 0x0a, 0x12,
	0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65,
	0x73, 0x73, 0x12, 0x2d, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76,
	0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63,
	0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31,
	0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x12, 0x6d, 0x0a, 0x16, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69,
	0x6e, 0x67, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x54, 0x79, 0x70, 0x65, 0x12, 0x31, 0x2e,
	0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x50, 0x72,
	0x6f, 0x63, 0x65, 0x73, 0x73, 0x54, 0x79, 0x70, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x67, 0x0a, 0x13, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e,
	0x67, 0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x12, 0x2e, 0x2e, 0x66, 0x63, 0x70, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c,
	0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x43, 0x61, 0x74, 0x65, 0x67, 0x6f,
	0x72, 0x79, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44,
	0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x61, 0x0a, 0x10, 0x4c,
	0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x42, 0x72, 0x61, 0x6e, 0x64, 0x12,
	0x2b, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67,
	0x42, 0x72, 0x61, 0x6e, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66,
	0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65,
	0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x65,
	0x0a, 0x12, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f,
	0x64, 0x75, 0x63, 0x74, 0x12, 0x2d, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72,
	0x69, 0x63, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e,
	0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x6f, 0x0a, 0x17, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69,
	0x63, 0x69, 0x6e, 0x67, 0x51, 0x75, 0x61, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x54, 0x79, 0x70, 0x65,
	0x12, 0x32, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e,
	0x67, 0x51, 0x75, 0x61, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x54, 0x79, 0x70, 0x65, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x6f, 0x0a, 0x17, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72,
	0x69, 0x63, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x47, 0x72, 0x6f, 0x75,
	0x70, 0x12, 0x32, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31,
	0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69,
	0x6e, 0x67, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65,
	0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x71, 0x0a, 0x14, 0x4c, 0x69, 0x73, 0x74, 0x50,
	0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x57, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x12,
	0x37, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67,
	0x57, 0x61, 0x72, 0x65, 0x68, 0x6f, 0x75, 0x73, 0x65, 0x4c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69,
	0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x79, 0x0a, 0x1c, 0x4c, 0x69,
	0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x44, 0x65, 0x6c, 0x69, 0x76, 0x65, 0x72,
	0x79, 0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x37, 0x2e, 0x66, 0x63, 0x70,
	0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e,
	0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x44, 0x65, 0x6c, 0x69, 0x76,
	0x65, 0x72, 0x79, 0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e,
	0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x6d, 0x0a, 0x10, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69,
	0x63, 0x69, 0x6e, 0x67, 0x4e, 0x61, 0x6d, 0x65, 0x73, 0x12, 0x2b, 0x2e, 0x66, 0x63, 0x70, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c,
	0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x4e, 0x61, 0x6d, 0x65, 0x73, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x2c, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74,
	0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x4e, 0x61, 0x6d, 0x65, 0x73, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x12, 0x6e, 0x0a, 0x13, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63,
	0x69, 0x6e, 0x67, 0x53, 0x75, 0x62, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x12, 0x35, 0x2e, 0x66, 0x63,
	0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x64,
	0x75, 0x63, 0x74, 0x53, 0x75, 0x62, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76,
	0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x12, 0x67, 0x0a, 0x13, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63,
	0x69, 0x6e, 0x67, 0x53, 0x6f, 0x72, 0x74, 0x54, 0x79, 0x70, 0x65, 0x12, 0x2e, 0x2e, 0x66, 0x63,
	0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x53, 0x6f, 0x72, 0x74,
	0x54, 0x79, 0x70, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63,
	0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x69, 0x0a,
	0x14, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x50, 0x6c, 0x61, 0x6e,
	0x74, 0x54, 0x79, 0x70, 0x65, 0x12, 0x2f, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65,
	0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50,
	0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x50, 0x6c, 0x61, 0x6e, 0x74, 0x54, 0x79, 0x70, 0x65, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74,
	0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x65, 0x0a, 0x12, 0x4c, 0x69, 0x73, 0x74,
	0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x53, 0x70, 0x65, 0x63, 0x69, 0x65, 0x73, 0x12, 0x2d,
	0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x53,
	0x70, 0x65, 0x63, 0x69, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e,
	0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x71, 0x0a, 0x18, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x4d, 0x61,
	0x74, 0x75, 0x72, 0x69, 0x74, 0x79, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x12, 0x33, 0x2e, 0x66, 0x63,
	0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x4d, 0x61, 0x74, 0x75,
	0x72, 0x69, 0x74, 0x79, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x69, 0x0a, 0x14, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e,
	0x67, 0x46, 0x72, 0x75, 0x69, 0x74, 0x46, 0x6f, 0x72, 0x6d, 0x12, 0x2f, 0x2e, 0x66, 0x63, 0x70,
	0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e,
	0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x46, 0x72, 0x75, 0x69, 0x74,
	0x46, 0x6f, 0x72, 0x6d, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63,
	0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x75, 0x0a,
	0x1a, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x50, 0x6f, 0x6c, 0x6c,
	0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x54, 0x79, 0x70, 0x65, 0x12, 0x35, 0x2e, 0x66, 0x63,
	0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x50, 0x6f, 0x6c, 0x6c,
	0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x54, 0x79, 0x70, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76,
	0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x12, 0x5b, 0x0a, 0x0a, 0x47, 0x65, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69,
	0x6e, 0x67, 0x12, 0x25, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76,
	0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x47, 0x65, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69,
	0x6e, 0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x26, 0x2e, 0x66, 0x63, 0x70, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x47,
	0x65, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x12, 0x5e, 0x0a, 0x0b, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67,
	0x12, 0x26, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e,
	0x67, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x27, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69,
	0x73, 0x74, 0x50, 0x72, 0x69, 0x63, 0x69, 0x6e, 0x67, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x12, 0x7a, 0x0a, 0x0f, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x50, 0x72, 0x69, 0x63, 0x65,
	0x4c, 0x69, 0x73, 0x74, 0x12, 0x32, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x70, 0x72, 0x69, 0x76, 0x61, 0x74,
	0x65, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x50, 0x72, 0x69, 0x63, 0x65, 0x4c, 0x69, 0x73,
	0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x33, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x70, 0x72,
	0x69, 0x76, 0x61, 0x74, 0x65, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x50, 0x72, 0x69, 0x63,
	0x65, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x7a, 0x0a,
	0x0f, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x50, 0x72, 0x69, 0x63, 0x65, 0x4c, 0x69, 0x73, 0x74,
	0x12, 0x32, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x70, 0x72, 0x69, 0x76, 0x61, 0x74, 0x65, 0x2e, 0x44, 0x65,
	0x6c, 0x65, 0x74, 0x65, 0x50, 0x72, 0x69, 0x63, 0x65, 0x4c, 0x69, 0x73, 0x74, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x33, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x70, 0x72, 0x69, 0x76, 0x61, 0x74,
	0x65, 0x2e, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x50, 0x72, 0x69, 0x63, 0x65, 0x4c, 0x69, 0x73,
	0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x5b, 0x0a, 0x1d, 0x63, 0x6f, 0x6d,
	0x2e, 0x66, 0x63, 0x70, 0x2e, 0x61, 0x70, 0x69, 0x73, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x5f, 0x70, 0x72, 0x69, 0x76, 0x61, 0x74, 0x65, 0x50, 0x01, 0x5a, 0x35, 0x67, 0x69,
	0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x66, 0x63, 0x70, 0x61, 0x72, 0x74, 0x6e,
	0x65, 0x72, 0x73, 0x2f, 0x61, 0x70, 0x69, 0x73, 0x2f, 0x67, 0x65, 0x6e, 0x2f, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x2f, 0x76, 0x31, 0x2f, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x70, 0x72, 0x69, 0x76,
	0x61, 0x74, 0x65, 0xba, 0x02, 0x00, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_v1_order_private_pricing_private_proto_rawDescOnce sync.Once
	file_v1_order_private_pricing_private_proto_rawDescData = file_v1_order_private_pricing_private_proto_rawDesc
)

func file_v1_order_private_pricing_private_proto_rawDescGZIP() []byte {
	file_v1_order_private_pricing_private_proto_rawDescOnce.Do(func() {
		file_v1_order_private_pricing_private_proto_rawDescData = protoimpl.X.CompressGZIP(file_v1_order_private_pricing_private_proto_rawDescData)
	})
	return file_v1_order_private_pricing_private_proto_rawDescData
}

var file_v1_order_private_pricing_private_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_v1_order_private_pricing_private_proto_goTypes = []interface{}{
	(*UpdatePriceListRequest)(nil),                    // 0: fcp.order.v1.order_private.UpdatePriceListRequest
	(*UpdatePriceListResponse)(nil),                   // 1: fcp.order.v1.order_private.UpdatePriceListResponse
	(*DeletePriceListRequest)(nil),                    // 2: fcp.order.v1.order_private.DeletePriceListRequest
	(*DeletePriceListResponse)(nil),                   // 3: fcp.order.v1.order_private.DeletePriceListResponse
	(order.ProductProcess)(0),                         // 4: fcp.order.v1.order.ProductProcess
	(order.ProductType)(0),                            // 5: fcp.order.v1.order.ProductType
	(order.Pricing_Status)(0),                         // 6: fcp.order.v1.order.Pricing.Status
	(*order.ListPricingProcessRequest)(nil),           // 7: fcp.order.v1.order.ListPricingProcessRequest
	(*order.ListPricingProcessTypeRequest)(nil),       // 8: fcp.order.v1.order.ListPricingProcessTypeRequest
	(*order.ListPricingCategoryRequest)(nil),          // 9: fcp.order.v1.order.ListPricingCategoryRequest
	(*order.ListPricingBrandRequest)(nil),             // 10: fcp.order.v1.order.ListPricingBrandRequest
	(*order.ListPricingProductRequest)(nil),           // 11: fcp.order.v1.order.ListPricingProductRequest
	(*order.ListPricingQuantityTypeRequest)(nil),      // 12: fcp.order.v1.order.ListPricingQuantityTypeRequest
	(*order.ListPricingProductGroupRequest)(nil),      // 13: fcp.order.v1.order.ListPricingProductGroupRequest
	(*order.ListPricingWarehouseLocationRequest)(nil), // 14: fcp.order.v1.order.ListPricingWarehouseLocationRequest
	(*order.ListPricingDeliveryConditionRequest)(nil), // 15: fcp.order.v1.order.ListPricingDeliveryConditionRequest
	(*order.ListPricingNamesRequest)(nil),             // 16: fcp.order.v1.order.ListPricingNamesRequest
	(*order.ListPricingProductSubGroupRequest)(nil),   // 17: fcp.order.v1.order.ListPricingProductSubGroupRequest
	(*order.ListPricingSortTypeRequest)(nil),          // 18: fcp.order.v1.order.ListPricingSortTypeRequest
	(*order.ListPricingPlantTypeRequest)(nil),         // 19: fcp.order.v1.order.ListPricingPlantTypeRequest
	(*order.ListPricingSpeciesRequest)(nil),           // 20: fcp.order.v1.order.ListPricingSpeciesRequest
	(*order.ListPricingMaturityGroupRequest)(nil),     // 21: fcp.order.v1.order.ListPricingMaturityGroupRequest
	(*order.ListPricingFruitFormRequest)(nil),         // 22: fcp.order.v1.order.ListPricingFruitFormRequest
	(*order.ListPricingPollinationTypeRequest)(nil),   // 23: fcp.order.v1.order.ListPricingPollinationTypeRequest
	(*order.GetPricingRequest)(nil),                   // 24: fcp.order.v1.order.GetPricingRequest
	(*order.ListPricingRequest)(nil),                  // 25: fcp.order.v1.order.ListPricingRequest
	(*order.DictResponse)(nil),                        // 26: fcp.order.v1.order.DictResponse
	(*order.ListPricingNamesResponse)(nil),            // 27: fcp.order.v1.order.ListPricingNamesResponse
	(*order.GetPricingResponse)(nil),                  // 28: fcp.order.v1.order.GetPricingResponse
	(*order.ListPricingResponse)(nil),                 // 29: fcp.order.v1.order.ListPricingResponse
}
var file_v1_order_private_pricing_private_proto_depIdxs = []int32{
	4,  // 0: fcp.order.v1.order_private.UpdatePriceListRequest.process:type_name -> fcp.order.v1.order.ProductProcess
	5,  // 1: fcp.order.v1.order_private.UpdatePriceListRequest.type:type_name -> fcp.order.v1.order.ProductType
	6,  // 2: fcp.order.v1.order_private.UpdatePriceListRequest.status:type_name -> fcp.order.v1.order.Pricing.Status
	4,  // 3: fcp.order.v1.order_private.DeletePriceListRequest.process:type_name -> fcp.order.v1.order.ProductProcess
	5,  // 4: fcp.order.v1.order_private.DeletePriceListRequest.type:type_name -> fcp.order.v1.order.ProductType
	7,  // 5: fcp.order.v1.order_private.PricingService.ListPricingProcess:input_type -> fcp.order.v1.order.ListPricingProcessRequest
	8,  // 6: fcp.order.v1.order_private.PricingService.ListPricingProcessType:input_type -> fcp.order.v1.order.ListPricingProcessTypeRequest
	9,  // 7: fcp.order.v1.order_private.PricingService.ListPricingCategory:input_type -> fcp.order.v1.order.ListPricingCategoryRequest
	10, // 8: fcp.order.v1.order_private.PricingService.ListPricingBrand:input_type -> fcp.order.v1.order.ListPricingBrandRequest
	11, // 9: fcp.order.v1.order_private.PricingService.ListPricingProduct:input_type -> fcp.order.v1.order.ListPricingProductRequest
	12, // 10: fcp.order.v1.order_private.PricingService.ListPricingQuantityType:input_type -> fcp.order.v1.order.ListPricingQuantityTypeRequest
	13, // 11: fcp.order.v1.order_private.PricingService.ListPricingProductGroup:input_type -> fcp.order.v1.order.ListPricingProductGroupRequest
	14, // 12: fcp.order.v1.order_private.PricingService.ListPricingWarehouse:input_type -> fcp.order.v1.order.ListPricingWarehouseLocationRequest
	15, // 13: fcp.order.v1.order_private.PricingService.ListPricingDeliveryCondition:input_type -> fcp.order.v1.order.ListPricingDeliveryConditionRequest
	16, // 14: fcp.order.v1.order_private.PricingService.ListPricingNames:input_type -> fcp.order.v1.order.ListPricingNamesRequest
	17, // 15: fcp.order.v1.order_private.PricingService.ListPricingSubGroup:input_type -> fcp.order.v1.order.ListPricingProductSubGroupRequest
	18, // 16: fcp.order.v1.order_private.PricingService.ListPricingSortType:input_type -> fcp.order.v1.order.ListPricingSortTypeRequest
	19, // 17: fcp.order.v1.order_private.PricingService.ListPricingPlantType:input_type -> fcp.order.v1.order.ListPricingPlantTypeRequest
	20, // 18: fcp.order.v1.order_private.PricingService.ListPricingSpecies:input_type -> fcp.order.v1.order.ListPricingSpeciesRequest
	21, // 19: fcp.order.v1.order_private.PricingService.ListPricingMaturityGroup:input_type -> fcp.order.v1.order.ListPricingMaturityGroupRequest
	22, // 20: fcp.order.v1.order_private.PricingService.ListPricingFruitForm:input_type -> fcp.order.v1.order.ListPricingFruitFormRequest
	23, // 21: fcp.order.v1.order_private.PricingService.ListPricingPollinationType:input_type -> fcp.order.v1.order.ListPricingPollinationTypeRequest
	24, // 22: fcp.order.v1.order_private.PricingService.GetPricing:input_type -> fcp.order.v1.order.GetPricingRequest
	25, // 23: fcp.order.v1.order_private.PricingService.ListPricing:input_type -> fcp.order.v1.order.ListPricingRequest
	0,  // 24: fcp.order.v1.order_private.PricingService.UpdatePriceList:input_type -> fcp.order.v1.order_private.UpdatePriceListRequest
	2,  // 25: fcp.order.v1.order_private.PricingService.DeletePriceList:input_type -> fcp.order.v1.order_private.DeletePriceListRequest
	26, // 26: fcp.order.v1.order_private.PricingService.ListPricingProcess:output_type -> fcp.order.v1.order.DictResponse
	26, // 27: fcp.order.v1.order_private.PricingService.ListPricingProcessType:output_type -> fcp.order.v1.order.DictResponse
	26, // 28: fcp.order.v1.order_private.PricingService.ListPricingCategory:output_type -> fcp.order.v1.order.DictResponse
	26, // 29: fcp.order.v1.order_private.PricingService.ListPricingBrand:output_type -> fcp.order.v1.order.DictResponse
	26, // 30: fcp.order.v1.order_private.PricingService.ListPricingProduct:output_type -> fcp.order.v1.order.DictResponse
	26, // 31: fcp.order.v1.order_private.PricingService.ListPricingQuantityType:output_type -> fcp.order.v1.order.DictResponse
	26, // 32: fcp.order.v1.order_private.PricingService.ListPricingProductGroup:output_type -> fcp.order.v1.order.DictResponse
	26, // 33: fcp.order.v1.order_private.PricingService.ListPricingWarehouse:output_type -> fcp.order.v1.order.DictResponse
	26, // 34: fcp.order.v1.order_private.PricingService.ListPricingDeliveryCondition:output_type -> fcp.order.v1.order.DictResponse
	27, // 35: fcp.order.v1.order_private.PricingService.ListPricingNames:output_type -> fcp.order.v1.order.ListPricingNamesResponse
	26, // 36: fcp.order.v1.order_private.PricingService.ListPricingSubGroup:output_type -> fcp.order.v1.order.DictResponse
	26, // 37: fcp.order.v1.order_private.PricingService.ListPricingSortType:output_type -> fcp.order.v1.order.DictResponse
	26, // 38: fcp.order.v1.order_private.PricingService.ListPricingPlantType:output_type -> fcp.order.v1.order.DictResponse
	26, // 39: fcp.order.v1.order_private.PricingService.ListPricingSpecies:output_type -> fcp.order.v1.order.DictResponse
	26, // 40: fcp.order.v1.order_private.PricingService.ListPricingMaturityGroup:output_type -> fcp.order.v1.order.DictResponse
	26, // 41: fcp.order.v1.order_private.PricingService.ListPricingFruitForm:output_type -> fcp.order.v1.order.DictResponse
	26, // 42: fcp.order.v1.order_private.PricingService.ListPricingPollinationType:output_type -> fcp.order.v1.order.DictResponse
	28, // 43: fcp.order.v1.order_private.PricingService.GetPricing:output_type -> fcp.order.v1.order.GetPricingResponse
	29, // 44: fcp.order.v1.order_private.PricingService.ListPricing:output_type -> fcp.order.v1.order.ListPricingResponse
	1,  // 45: fcp.order.v1.order_private.PricingService.UpdatePriceList:output_type -> fcp.order.v1.order_private.UpdatePriceListResponse
	3,  // 46: fcp.order.v1.order_private.PricingService.DeletePriceList:output_type -> fcp.order.v1.order_private.DeletePriceListResponse
	26, // [26:47] is the sub-list for method output_type
	5,  // [5:26] is the sub-list for method input_type
	5,  // [5:5] is the sub-list for extension type_name
	5,  // [5:5] is the sub-list for extension extendee
	0,  // [0:5] is the sub-list for field type_name
}

func init() { file_v1_order_private_pricing_private_proto_init() }
func file_v1_order_private_pricing_private_proto_init() {
	if File_v1_order_private_pricing_private_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_v1_order_private_pricing_private_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdatePriceListRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_order_private_pricing_private_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdatePriceListResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_order_private_pricing_private_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeletePriceListRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_order_private_pricing_private_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeletePriceListResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_v1_order_private_pricing_private_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_v1_order_private_pricing_private_proto_goTypes,
		DependencyIndexes: file_v1_order_private_pricing_private_proto_depIdxs,
		MessageInfos:      file_v1_order_private_pricing_private_proto_msgTypes,
	}.Build()
	File_v1_order_private_pricing_private_proto = out.File
	file_v1_order_private_pricing_private_proto_rawDesc = nil
	file_v1_order_private_pricing_private_proto_goTypes = nil
	file_v1_order_private_pricing_private_proto_depIdxs = nil
}
