// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package order_mobile

import (
	context "context"
	order "gitlab.com/fcpartners/apis/gen/order/v1/order"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// MultiDealServiceClient is the client API for MultiDealService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type MultiDealServiceClient interface {
	//    rpc ListMultiDealProcess (order.ListMultiDealProcessRequest) returns (order.DictResponse);
	ListMultiDealProcessType(ctx context.Context, in *order.ListMultiDealProcessTypeRequest, opts ...grpc.CallOption) (*order.DictResponse, error)
	GetMultiDeal(ctx context.Context, in *order.GetMultiDealRequest, opts ...grpc.CallOption) (*order.GetMultiDealResponse, error)
	ListMultiDeal(ctx context.Context, in *order.ListMultiDealRequest, opts ...grpc.CallOption) (*order.ListMultiDealResponse, error)
	GetMultiDealContact(ctx context.Context, in *order.GetMultiDealContactRequest, opts ...grpc.CallOption) (*order.GetMultiDealContactResponse, error)
	ListMultiDealContact(ctx context.Context, in *order.ListMultiDealContactRequest, opts ...grpc.CallOption) (*order.ListMultiDealContactResponse, error)
	GetMultiDealFile(ctx context.Context, in *order.GetMultiDealFileRequest, opts ...grpc.CallOption) (*order.GetMultiDealFileResponse, error)
	ListMultiDealFile(ctx context.Context, in *order.ListMultiDealFileRequest, opts ...grpc.CallOption) (*order.ListMultiDealFileResponse, error)
	AddMultiDealFile(ctx context.Context, in *order.AddMultiDealFileRequest, opts ...grpc.CallOption) (*order.AddMultiDealFileResponse, error)
	DeleteMultiDealFile(ctx context.Context, in *order.DeleteMultiDealFileRequest, opts ...grpc.CallOption) (*order.DeleteMultiDealFileResponse, error)
	DoneMultiDeal(ctx context.Context, in *DoneMultiDealRequest, opts ...grpc.CallOption) (*DoneMultiDealResponse, error)
}

type multiDealServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewMultiDealServiceClient(cc grpc.ClientConnInterface) MultiDealServiceClient {
	return &multiDealServiceClient{cc}
}

func (c *multiDealServiceClient) ListMultiDealProcessType(ctx context.Context, in *order.ListMultiDealProcessTypeRequest, opts ...grpc.CallOption) (*order.DictResponse, error) {
	out := new(order.DictResponse)
	err := c.cc.Invoke(ctx, "/fcp.order.v1.order_mobile.MultiDealService/ListMultiDealProcessType", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *multiDealServiceClient) GetMultiDeal(ctx context.Context, in *order.GetMultiDealRequest, opts ...grpc.CallOption) (*order.GetMultiDealResponse, error) {
	out := new(order.GetMultiDealResponse)
	err := c.cc.Invoke(ctx, "/fcp.order.v1.order_mobile.MultiDealService/GetMultiDeal", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *multiDealServiceClient) ListMultiDeal(ctx context.Context, in *order.ListMultiDealRequest, opts ...grpc.CallOption) (*order.ListMultiDealResponse, error) {
	out := new(order.ListMultiDealResponse)
	err := c.cc.Invoke(ctx, "/fcp.order.v1.order_mobile.MultiDealService/ListMultiDeal", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *multiDealServiceClient) GetMultiDealContact(ctx context.Context, in *order.GetMultiDealContactRequest, opts ...grpc.CallOption) (*order.GetMultiDealContactResponse, error) {
	out := new(order.GetMultiDealContactResponse)
	err := c.cc.Invoke(ctx, "/fcp.order.v1.order_mobile.MultiDealService/GetMultiDealContact", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *multiDealServiceClient) ListMultiDealContact(ctx context.Context, in *order.ListMultiDealContactRequest, opts ...grpc.CallOption) (*order.ListMultiDealContactResponse, error) {
	out := new(order.ListMultiDealContactResponse)
	err := c.cc.Invoke(ctx, "/fcp.order.v1.order_mobile.MultiDealService/ListMultiDealContact", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *multiDealServiceClient) GetMultiDealFile(ctx context.Context, in *order.GetMultiDealFileRequest, opts ...grpc.CallOption) (*order.GetMultiDealFileResponse, error) {
	out := new(order.GetMultiDealFileResponse)
	err := c.cc.Invoke(ctx, "/fcp.order.v1.order_mobile.MultiDealService/GetMultiDealFile", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *multiDealServiceClient) ListMultiDealFile(ctx context.Context, in *order.ListMultiDealFileRequest, opts ...grpc.CallOption) (*order.ListMultiDealFileResponse, error) {
	out := new(order.ListMultiDealFileResponse)
	err := c.cc.Invoke(ctx, "/fcp.order.v1.order_mobile.MultiDealService/ListMultiDealFile", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *multiDealServiceClient) AddMultiDealFile(ctx context.Context, in *order.AddMultiDealFileRequest, opts ...grpc.CallOption) (*order.AddMultiDealFileResponse, error) {
	out := new(order.AddMultiDealFileResponse)
	err := c.cc.Invoke(ctx, "/fcp.order.v1.order_mobile.MultiDealService/AddMultiDealFile", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *multiDealServiceClient) DeleteMultiDealFile(ctx context.Context, in *order.DeleteMultiDealFileRequest, opts ...grpc.CallOption) (*order.DeleteMultiDealFileResponse, error) {
	out := new(order.DeleteMultiDealFileResponse)
	err := c.cc.Invoke(ctx, "/fcp.order.v1.order_mobile.MultiDealService/DeleteMultiDealFile", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *multiDealServiceClient) DoneMultiDeal(ctx context.Context, in *DoneMultiDealRequest, opts ...grpc.CallOption) (*DoneMultiDealResponse, error) {
	out := new(DoneMultiDealResponse)
	err := c.cc.Invoke(ctx, "/fcp.order.v1.order_mobile.MultiDealService/DoneMultiDeal", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MultiDealServiceServer is the server API for MultiDealService service.
// All implementations must embed UnimplementedMultiDealServiceServer
// for forward compatibility
type MultiDealServiceServer interface {
	//    rpc ListMultiDealProcess (order.ListMultiDealProcessRequest) returns (order.DictResponse);
	ListMultiDealProcessType(context.Context, *order.ListMultiDealProcessTypeRequest) (*order.DictResponse, error)
	GetMultiDeal(context.Context, *order.GetMultiDealRequest) (*order.GetMultiDealResponse, error)
	ListMultiDeal(context.Context, *order.ListMultiDealRequest) (*order.ListMultiDealResponse, error)
	GetMultiDealContact(context.Context, *order.GetMultiDealContactRequest) (*order.GetMultiDealContactResponse, error)
	ListMultiDealContact(context.Context, *order.ListMultiDealContactRequest) (*order.ListMultiDealContactResponse, error)
	GetMultiDealFile(context.Context, *order.GetMultiDealFileRequest) (*order.GetMultiDealFileResponse, error)
	ListMultiDealFile(context.Context, *order.ListMultiDealFileRequest) (*order.ListMultiDealFileResponse, error)
	AddMultiDealFile(context.Context, *order.AddMultiDealFileRequest) (*order.AddMultiDealFileResponse, error)
	DeleteMultiDealFile(context.Context, *order.DeleteMultiDealFileRequest) (*order.DeleteMultiDealFileResponse, error)
	DoneMultiDeal(context.Context, *DoneMultiDealRequest) (*DoneMultiDealResponse, error)
	mustEmbedUnimplementedMultiDealServiceServer()
}

// UnimplementedMultiDealServiceServer must be embedded to have forward compatible implementations.
type UnimplementedMultiDealServiceServer struct {
}

func (UnimplementedMultiDealServiceServer) ListMultiDealProcessType(context.Context, *order.ListMultiDealProcessTypeRequest) (*order.DictResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListMultiDealProcessType not implemented")
}
func (UnimplementedMultiDealServiceServer) GetMultiDeal(context.Context, *order.GetMultiDealRequest) (*order.GetMultiDealResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMultiDeal not implemented")
}
func (UnimplementedMultiDealServiceServer) ListMultiDeal(context.Context, *order.ListMultiDealRequest) (*order.ListMultiDealResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListMultiDeal not implemented")
}
func (UnimplementedMultiDealServiceServer) GetMultiDealContact(context.Context, *order.GetMultiDealContactRequest) (*order.GetMultiDealContactResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMultiDealContact not implemented")
}
func (UnimplementedMultiDealServiceServer) ListMultiDealContact(context.Context, *order.ListMultiDealContactRequest) (*order.ListMultiDealContactResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListMultiDealContact not implemented")
}
func (UnimplementedMultiDealServiceServer) GetMultiDealFile(context.Context, *order.GetMultiDealFileRequest) (*order.GetMultiDealFileResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMultiDealFile not implemented")
}
func (UnimplementedMultiDealServiceServer) ListMultiDealFile(context.Context, *order.ListMultiDealFileRequest) (*order.ListMultiDealFileResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListMultiDealFile not implemented")
}
func (UnimplementedMultiDealServiceServer) AddMultiDealFile(context.Context, *order.AddMultiDealFileRequest) (*order.AddMultiDealFileResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddMultiDealFile not implemented")
}
func (UnimplementedMultiDealServiceServer) DeleteMultiDealFile(context.Context, *order.DeleteMultiDealFileRequest) (*order.DeleteMultiDealFileResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteMultiDealFile not implemented")
}
func (UnimplementedMultiDealServiceServer) DoneMultiDeal(context.Context, *DoneMultiDealRequest) (*DoneMultiDealResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DoneMultiDeal not implemented")
}
func (UnimplementedMultiDealServiceServer) mustEmbedUnimplementedMultiDealServiceServer() {}

// UnsafeMultiDealServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to MultiDealServiceServer will
// result in compilation errors.
type UnsafeMultiDealServiceServer interface {
	mustEmbedUnimplementedMultiDealServiceServer()
}

func RegisterMultiDealServiceServer(s grpc.ServiceRegistrar, srv MultiDealServiceServer) {
	s.RegisterService(&MultiDealService_ServiceDesc, srv)
}

func _MultiDealService_ListMultiDealProcessType_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(order.ListMultiDealProcessTypeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MultiDealServiceServer).ListMultiDealProcessType(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.order.v1.order_mobile.MultiDealService/ListMultiDealProcessType",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MultiDealServiceServer).ListMultiDealProcessType(ctx, req.(*order.ListMultiDealProcessTypeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MultiDealService_GetMultiDeal_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(order.GetMultiDealRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MultiDealServiceServer).GetMultiDeal(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.order.v1.order_mobile.MultiDealService/GetMultiDeal",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MultiDealServiceServer).GetMultiDeal(ctx, req.(*order.GetMultiDealRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MultiDealService_ListMultiDeal_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(order.ListMultiDealRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MultiDealServiceServer).ListMultiDeal(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.order.v1.order_mobile.MultiDealService/ListMultiDeal",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MultiDealServiceServer).ListMultiDeal(ctx, req.(*order.ListMultiDealRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MultiDealService_GetMultiDealContact_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(order.GetMultiDealContactRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MultiDealServiceServer).GetMultiDealContact(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.order.v1.order_mobile.MultiDealService/GetMultiDealContact",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MultiDealServiceServer).GetMultiDealContact(ctx, req.(*order.GetMultiDealContactRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MultiDealService_ListMultiDealContact_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(order.ListMultiDealContactRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MultiDealServiceServer).ListMultiDealContact(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.order.v1.order_mobile.MultiDealService/ListMultiDealContact",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MultiDealServiceServer).ListMultiDealContact(ctx, req.(*order.ListMultiDealContactRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MultiDealService_GetMultiDealFile_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(order.GetMultiDealFileRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MultiDealServiceServer).GetMultiDealFile(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.order.v1.order_mobile.MultiDealService/GetMultiDealFile",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MultiDealServiceServer).GetMultiDealFile(ctx, req.(*order.GetMultiDealFileRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MultiDealService_ListMultiDealFile_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(order.ListMultiDealFileRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MultiDealServiceServer).ListMultiDealFile(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.order.v1.order_mobile.MultiDealService/ListMultiDealFile",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MultiDealServiceServer).ListMultiDealFile(ctx, req.(*order.ListMultiDealFileRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MultiDealService_AddMultiDealFile_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(order.AddMultiDealFileRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MultiDealServiceServer).AddMultiDealFile(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.order.v1.order_mobile.MultiDealService/AddMultiDealFile",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MultiDealServiceServer).AddMultiDealFile(ctx, req.(*order.AddMultiDealFileRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MultiDealService_DeleteMultiDealFile_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(order.DeleteMultiDealFileRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MultiDealServiceServer).DeleteMultiDealFile(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.order.v1.order_mobile.MultiDealService/DeleteMultiDealFile",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MultiDealServiceServer).DeleteMultiDealFile(ctx, req.(*order.DeleteMultiDealFileRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _MultiDealService_DoneMultiDeal_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DoneMultiDealRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MultiDealServiceServer).DoneMultiDeal(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/fcp.order.v1.order_mobile.MultiDealService/DoneMultiDeal",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MultiDealServiceServer).DoneMultiDeal(ctx, req.(*DoneMultiDealRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// MultiDealService_ServiceDesc is the grpc.ServiceDesc for MultiDealService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var MultiDealService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "fcp.order.v1.order_mobile.MultiDealService",
	HandlerType: (*MultiDealServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ListMultiDealProcessType",
			Handler:    _MultiDealService_ListMultiDealProcessType_Handler,
		},
		{
			MethodName: "GetMultiDeal",
			Handler:    _MultiDealService_GetMultiDeal_Handler,
		},
		{
			MethodName: "ListMultiDeal",
			Handler:    _MultiDealService_ListMultiDeal_Handler,
		},
		{
			MethodName: "GetMultiDealContact",
			Handler:    _MultiDealService_GetMultiDealContact_Handler,
		},
		{
			MethodName: "ListMultiDealContact",
			Handler:    _MultiDealService_ListMultiDealContact_Handler,
		},
		{
			MethodName: "GetMultiDealFile",
			Handler:    _MultiDealService_GetMultiDealFile_Handler,
		},
		{
			MethodName: "ListMultiDealFile",
			Handler:    _MultiDealService_ListMultiDealFile_Handler,
		},
		{
			MethodName: "AddMultiDealFile",
			Handler:    _MultiDealService_AddMultiDealFile_Handler,
		},
		{
			MethodName: "DeleteMultiDealFile",
			Handler:    _MultiDealService_DeleteMultiDealFile_Handler,
		},
		{
			MethodName: "DoneMultiDeal",
			Handler:    _MultiDealService_DoneMultiDeal_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "v1/order_mobile/multi_deal_mobile.proto",
}
